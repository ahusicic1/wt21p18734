const Sequelize = require("sequelize");
const sequelize = require("./db.js");
const express = require("express");
const multer = require('multer');
const csv = require("csv-parser");
const bodyParser = require("body-parser");
const fs = require("fs");
const app = express();
/*const fileStorageEngine = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, )
    }
});
const upload = multer({storage: fileStorageEngine});*/

const Vjezba = require("./models/Vjezba.js")(sequelize);
const Zadatak = require("./models/Zadatak.js")(sequelize);
const Grupa = require("./models/Grupa.js")(sequelize);
const Student = require("./models/Student.js")(sequelize);

sequelize
  .sync()
  .then((result) => {
    console.log(result);
  })
  .catch((err) => {
    console.log(err);
  });

//relacije
// Veza 1-n vise studenata moze biti u jednoj grupi
Grupa.hasMany(Student, { as: "studentiGrupe" });
Vjezba.hasMany(Zadatak, { as: "zadaciVjezbe" });

app.use(bodyParser.json());
app.use(bodyParser.text())
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.urlencoded({ extended: false }));
app.use(express.static("public/html"));
app.use(express.static("public/css"));
app.use(express.static("public/js"));
app.use(express.static("public/images"));

app.get("/vjezbe/", function (req, res) {
  var json = "";
  var brojVjezbi = 0;
  Vjezba.findAll()
    .then((rez) => {
      json += '{"brojVjezbi":"' + rez.length + '", "brojZadataka": [';
      brojVjezbi = rez.length;
      return Zadatak.findAll();
    })
    .then((data) => {
        
        console.log("broj Vjezbi je: "+brojVjezbi);
        console.log("poslano zadataka: "+data.length);
      for (let i = 0; i < brojVjezbi; i++) {
        var zadatakaNaVjezbi = data.filter((d) => {
          return ((d.VjezbaId).toString()).localeCompare((i+1).toString()) == 0;
         // console.log("d.VjezbaID je " + d.VjezbaId + ", a i+1 je " + (i + 1));
         // console.log(" konacno: " + parseInt(d.VjezbaId) == (i + 1) );
        }).length;
        console.log("zadatakaNaVjezbi: " + zadatakaNaVjezbi);
        json += zadatakaNaVjezbi;

        if (i + 1 < brojVjezbi) json += ",";
      }

      json += "] }";
      console.log(JSON.parse(json));
      res.send(JSON.parse(json));
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving Vjezbe.",
      });
    });
});

app.post("/vjezbe", function (req, res) {
  let tijelo = req.body;
  let brVjezbi = tijelo["brojVjezbi"];
  let brZadatakaLength = tijelo["brojZadataka"].length;
  let greska = 0;
  var json = "";
  var tekst = "Pogrešan parametar ";
  var naziviVjezbi = [];
  var naziviZadataka = [];
  json += '{"brojVjezbi":"' + brVjezbi + '", "brojZadataka": [';

  if (brVjezbi < 1 || brVjezbi > 15) {
    greska = 1;
    tekst += "brojVjezbi";
  }
  let zadaci = tijelo["brojZadataka"];

  for (let i = 0; i < tijelo.brojZadataka.length; i++) {
    if (zadaci[i] < 0 || zadaci[i] > 10) {
      if (greska != 0) {
        tekst += ",";
      }
      tekst = tekst + "z" + i;
      greska = 13;
    }
    json += zadaci[i];

    if (i + 1 < tijelo.brojZadataka.length) json += ",";
  }
  json += "] }";

  if (brZadatakaLength != brVjezbi) {
    if (greska != 0) {
      tekst += ",";
    }
    greska = 12;
    tekst += "brojZadataka";
  }

  if (greska != 0) {
    let obj = {
      status: "error",
      data: tekst,
    };
    res.send(obj);
    return;
  } else {
    for (let i = 0; i < brVjezbi; i++) {
      var s = "Vjezba" + (i + 1).toString();
      naziviVjezbi.push({ naziv: s });
    }
    var zadaciVjezbe = [];
  
    Zadatak.destroy({
        where: {},
        cascade: true
    }).then(()=>{
        return Vjezba.destroy({
            where: {},
            cascade: true
        })
    }).then(()=>{

    Vjezba.bulkCreate(naziviVjezbi)
      .then((data) => {
        console.log("Kreirano! Data: " + data.toString());
        for (let i = 0; i < data.length; i++) {
          console.log(
            "Data[i]:" + data[i].id + " data[i].naziv: " + data[i].naziv
          );
        }
        return Vjezba.findAll();
      })
      .then((sveVjezbe) => {
        console.log("sveVjezbe:" + sveVjezbe);
        var zadaciVjezbe = [];
        for (let i = 0; i < sveVjezbe.length; i++) {
          var zadatakaNaTrenutnojVjezbi = zadaci[i];
          for (let j = 0; j < zadatakaNaTrenutnojVjezbi; j++) {
            var t = "z" + (j + 1).toString();
            zadaciVjezbe.push({ tekst: t, VjezbaId: sveVjezbe[i].id });
          }
        }

        return Zadatak.bulkCreate(zadaciVjezbe);
      })
      .then((result) => {
        console.log("Uspjesno upisane vjezbe i njima odgovarajuci zadaci");
        res.send(JSON.parse(json));
      })
      .catch((err) => {
        res.status(500).send({
          message: err.message || "Some error occurred while creating Vjezbe.",
        });
      });
    ///
    /*console.log("Primljen je post zahtjev");
            res.writeHead(200, {'Content-Type': 'application/json'});
            var objectBody = req.body;
            res.write(JSON.stringify(objectBody));
            res.end();*/
        }); 
    }
    });



app.post("/student", function (req, res) {
    console.log("POST student");
    console.log("req.body:"+req.body);
      var tijeloZahtjeva = req.body;
      Student.findAll({ where: { index: tijeloZahtjeva.index } }).then(function(provjeraStudenta){
      if(provjeraStudenta.length!=0){
          var odgovorStatus = { status: ''};
          odgovorStatus.status = 'Student sa indexom '+ tijeloZahtjeva.index +' već postoji!';
          res.writeHead(200, { 'Content-type': 'application/json' });
          res.write(JSON.stringify(odgovorStatus));
          res.end();
          //res.send(JSON.parse('{"status":"Student sa indexom {'+ tijeloZahtjeva.index +'} već postoji!"}'));
         // return;
      }
     else{
      var student = null;
      var obj = {ime: tijeloZahtjeva.ime,
        prezime: tijeloZahtjeva.prezime,
        index: tijeloZahtjeva.index,
        GrupaId:null };

    Student.create({ime: obj.ime, prezime: obj.prezime, index: obj.index, GrupaId: null}).then(function(s){
           student = s;
         return  Grupa.findAll({ where: { naziv:tijeloZahtjeva.grupa.toString() } });
        }).then(function(nizGrupa){
            if(nizGrupa.length!=0){
               //  student.set({GrupaId:nizGrupa[0].id});
                 console.log("Grupa postoji");
                 return nizGrupa[0];
            }
            else{
                console.log("Grupa ne postoji i pravi se nova");
                return  Grupa.create({naziv:tijeloZahtjeva.grupa });
            }
        }).then(function(g){
            console.log("g.id = "+g.id);
            return Student.update({ GrupaId: g.id },
            { where: { id: student.id } });
        }).then(function(st){
            var odgovorStatus = { status: 'Kreiran student!'};
            res.writeHead(200, { 'Content-Type': 'application/json' });
            res.write(JSON.stringify(odgovorStatus));
            res.end();
           // res.status(400).send(JSON.parse('{"status":"Kreiran student!"}'));
           // res.send(JSON.parse('{"status":"Kreiran student!"}'));
            //return;
        }).catch((err) => {
            res.status(500).send({
              message: err.message || "Some error occurred while creating Vjezbe.",
            });
          }); 

        }
});
});

app.put("/student/:index", function (req, res) {
   var tijeloZahtjeva = req.body;
   Student.findAll({ where: { index:req.params.index } }).then(function(x){
         if(x.length == 0){
             var poruka = {status:''};
             poruka.status = "Student sa indexom "+ req.params.index +" ne postoji";
             res.send(JSON.stringify(poruka));
             return;
         }
        else{
            return Grupa.findAll({ where: { naziv:tijeloZahtjeva.grupa } } ).then(function(grupe){
       if(grupe.length == 0){
           return Grupa.create({naziv:tijeloZahtjeva.grupa});
       }
       else{
           return grupe[0];
       }
       }).then(function(gr){
          return Student.update({ GrupaId: gr.id },
            { where: { index:req.params.index } });
       }).then(function(student){
        var poruka = '{"status":"Promijenjena grupa studentu '+ req.params.index +'"}';
        res.send(JSON.parse(poruka));
       // return;
       }).catch((err) => {
        res.status(500).send({
          message: err.message || "Some error occurred while creating Vjezbe.",
        });
      }); 
    }
})

});

app.post("/batch/student", function (req, res) {
    var tijeloZahtjeva = req.body;
     var listaPromisea = [];
            
     var str = tijeloZahtjeva.toString('utf-8');
     console.log("str=",str);
     console.log("tijeloZahtjeva:",tijeloZahtjeva);
     var niz = str.split(/[\r\n]/).filter(s => s.length >0);
     var brojPoslanihStudenata = niz.length;
    console.log("poslano redova:"+brojPoslanihStudenata);
    var indexiPostojecih = [];
    var odgovor = "Dodano ";

    var studentiIzBaze = [];
    var studentiZaDodati = [];
    var grupeZaDodati = [];
    var naziviGrupaUZahtjevu = [];
    var sveGrupe = [];
    Student.findAll().then(function(studenti){
        for(let i=0; i<studenti.length; i++){
            studentiIzBaze.push(studenti[i].index);
        }
        return studenti;
    }).then(function(studenti){
        for(let i=0; i<brojPoslanihStudenata; i++){
            var nizPolja = niz[i].split(/[\r\n,]/).filter(s => s.length >0);
            var postojiLi = studenti.filter(s=>{
                console.log("s.index je " + s.index + ", a nizPolja[2] je " + nizPolja[2] );
                console.log(s.index == nizPolja[2]);
                 return s.index == nizPolja[2];
            });
            var postojiLiUZahtjevu = studentiZaDodati.filter(s=>{
                 return s.index == nizPolja[2];
            });
            console.log("postojiLi.length = " + postojiLi.length);
            if(postojiLi.length!=0 || postojiLiUZahtjevu.length != 0){
                indexiPostojecih.push(nizPolja[2]);
            }
            else{
                 studentiZaDodati.push({ime:nizPolja[0], prezime: nizPolja[1], index: nizPolja[2], GrupaId: null});
                 naziviGrupaUZahtjevu.push(nizPolja[3]);
            }
        }

        return Grupa.findAll();
    }).then(function(grupeBaze){
        sveGrupe = grupeBaze;
        for(let i=0; i<naziviGrupaUZahtjevu.length; i++){
            var postojecaGrupa = grupeBaze.filter(g=>{
                console.log("g="+g+",nazivUZahtj[i]="+naziviGrupaUZahtjevu[i]);
                console.log(g.naziv == naziviGrupaUZahtjevu[i]);
             return g.naziv == naziviGrupaUZahtjevu[i];
            });

            if(postojecaGrupa.length == 0 && grupeZaDodati.find(g =>{ return g.naziv==naziviGrupaUZahtjevu[i]})==null){
                grupeZaDodati.push({naziv:naziviGrupaUZahtjevu[i]});
            }

        }   
        for(let i =0; i<grupeZaDodati.length; i++) console.log("dodaje se "+grupeZaDodati[i].naziv);
       return Grupa.bulkCreate(grupeZaDodati);
    }).then(function(){
        return Grupa.findAll();
    }).then(function(data){
        console.log("data je "+data);
        sveGrupe = data;
        console.log("sveGr.length = "+sveGrupe.length+", studentizadodati.length="+studentiZaDodati.length);
          for(let i=0; i<studentiZaDodati.length; i++){
              var idGrupe = sveGrupe.find(g => {
                  console.log("g.naziv="+g.naziv+", nazuvUZahtj[i]="+naziviGrupaUZahtjevu[i]);
                  console.log(g.naziv == naziviGrupaUZahtjevu[i]);
                   return g.naziv == naziviGrupaUZahtjevu[i];
              }).id;
              if(sveGrupe.length==0){
                  idGrupe=1;
              }
              console.log("idGrupe="+idGrupe);
                studentiZaDodati[i].GrupaId = idGrupe;
          }

          return Student.bulkCreate(studentiZaDodati);
    }).then(function(data){
        odgovor+=studentiZaDodati.length;
        odgovor+= " studenata";
        if(studentiZaDodati.length == brojPoslanihStudenata){
            odgovor+="!";
        }
        else{
            odgovor+=", a studenti ";
            for(let i=0; i<indexiPostojecih.length; i++){
                odgovor+=indexiPostojecih[i];
                if(i!=(indexiPostojecih.length - 1)){
                    odgovor+=", ";
                } 
            }
            odgovor+=" već postoje!";
        }
        var poruka = '{"status":"'+ odgovor +'"}';
        res.send(JSON.parse(poruka));

    }).catch((err) => {
        res.status(500).send({
          message: err.message || "Some error occurred while creating Vjezbe.",
        });
      }); 

    });
    


app.listen(3000);
