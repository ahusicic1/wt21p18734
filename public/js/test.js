let assert=chai.assert
chai.should();

let div=document.getElementById("dodajInputPolja");
describe('test', function() {

  beforeEach(function() {
    this.xhr = sinon.useFakeXMLHttpRequest();
 
    this.requests = [];
    this.xhr.onCreate = function(xhr) {
      this.requests.push(xhr);
    }.bind(this);
  });
 
  afterEach(function() {
    this.xhr.restore();
    while (div.firstChild) {
      div.removeChild(div.firstChild);
  }
  document.getElementById("odgServera").textContent="";
  document.getElementById("greska").textContent="";

  });
 

  it('Test dohvatanja podataka', function(done) {

    var podaci = {brojVjezbi: 5, brojZadataka: [1,2,3,4,5]}

    var dataJson = JSON.stringify(podaci)
    console.log(dataJson);
    VjezbeAjax.dohvatiPodatke(function(err,data) {

     // console.log("data je: " + data);
      var dataParse= JSON.parse(data);
        assert.equal(dataParse.brojVjezbi,5)
        assert.equal(dataParse.brojZadataka[0],1)
        assert.equal(dataParse.brojZadataka[1],2)
        assert.equal(dataParse.brojZadataka[2],3)
        assert.equal(dataParse.brojZadataka[3],4)
        assert.equal(dataParse.brojZadataka[4],5)

        done()
       
    })
   this.requests[0].respond(200,{'Content-Type':'text/json'}, dataJson)

  })
  
  it('Test slanja podataka', function(done) {

    var podaci = {brojVjezbi: 5, brojZadataka: [1,1,2,2,5]}
    var dataJson = JSON.stringify(podaci)
    VjezbeAjax.dodajInputPolja(div,5);

    VjezbeAjax.posaljiPodatke(div,function(err,data) {
      if(data){
      assert.equal(JSON.parse(data).brojVjezbi,5)
      }
      else{
        assert.isNull(data);
      }
      done()

  })
  this.requests[0].respond(200,{'Content-Type':'text/json'}, dataJson)
  })

    
  it('Tesitranje ispravnog input polja', function() {
    VjezbeAjax.dodajInputPolja(div,0);
   console.log("ima djece: "+div.children.length);
   assert.equal(div.children.length,0);
  })

  it('Testiranje neispravnog input polja', function() {
    
    VjezbeAjax.dodajInputPolja(div,80);
    assert.equal(div.children.length,0);  

  })


  it('Testiranje - nema iscrtavanja ako su parametri neispravni', function() { 

    var podaci = {brojVjezbi: 0, brojZadataka: [1,2]}
    var dataJson = JSON.stringify(podaci)
    VjezbeAjax.iscrtajVjezbe(div,podaci)
    assert.equal(div.children.length,0)  
    
  })
  it('Testiranje iscrtavanja vjezbe funkcije', function() { 

    var podaci = {brojVjezbi: 5, brojZadataka: [1,2,3,4,5]}
    var dataJson = JSON.stringify(podaci)
    VjezbeAjax.iscrtajVjezbe(div,podaci)
    assert.equal(div.children.length,5)   
    
  })

  

});