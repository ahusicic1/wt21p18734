var VjezbeAjax = (function () {    

    var dodajInputPolja = function (DOMelementDIVauFormi, brojVjezbi) { 
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
          if (xhttp.readyState == 4 && xhttp.status == 200) {
              console.log("status je ok");
             var container = DOMelementDIVauFormi;
             while (container.hasChildNodes()) {
                 container.removeChild(container.lastChild);
             }
           
             for (let i=0;i<brojVjezbi;i++){
                 container.appendChild(document.createTextNode("Broj zadataka vjezbe " + (i+1)));
                 var input = document.createElement("input");
                 input.type = "number";
                 input.name = "z" + i;
                 input.id = "z" + i;
                 input.value = 4;
                 container.appendChild(input);
                 container.appendChild(document.createElement("br"));
             }
          }
          else{
            console.log("Status nije ok");
          }
        };
        xhttp.open("GET", "unosVjezbi.html", true);
        xhttp.send();
    };

    var posaljiPodatke = function (vjezbeObjekat,callbackFja) { 
        var xhttp = new XMLHttpRequest();
        xhttp.open("POST", "http://localhost:3000/vjezbe", true);
        xhttp.setRequestHeader("Content-Type", "application/json");
        xhttp.onreadystatechange = function() {
          if (xhttp.readyState == 4 && xhttp.status == 200) {
           console.log("posalji podatke OK");
           var xhttpJSON = JSON.parse(xhttp.responseText);
           callbackFja(null, xhttpJSON);
          }
          else{
            console.log("Status nije ok");
            console.log(JSON.stringify(vjezbeObjekat));
            callbackFja(xhttp.response, null);
          }
        };
        console.log("sad ide za vjezbe send");
        xhttp.send(JSON.stringify(vjezbeObjekat));
    };

    var dohvatiPodatke = function (callbackFja) { 
        console.log("dohvatiPodatke pozvana");
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (xhttp.readyState == 4 && xhttp.status == 200) {
                console.log("dohvati podatke OK\n xhttp.responseText: " + xhttp.responseText+"\n response:"+xhttp.response+
                "callbackFja:" + callbackFja);
            
                callbackFja(null, xhttp.response);
               }
             if(xhttp.readyState == 4 && xhttp.status == 404){
                 console.log("dohvati podatke - status nije ok");
                 callbackFja(xhttp.response, null);
               }
        };
        
        xhttp.open("GET", "/vjezbe", true);
        xhttp.send();
    };

   
    var iscrtajVjezbe = function(divDOMelement,vjezbeObj){
   // provjera parametara
   var brVjezbi = vjezbeObj.brojVjezbi;
        let brZadatakaLength = vjezbeObj.brojZadataka.length;
        let greska = 0;
        var tekst = "Pogrešan parametar ";

        if(brVjezbi<1 || brVjezbi>15){
            greska = 1;
            tekst+="brojVjezbi";
        }
        let zadaci = vjezbeObj.brojZadataka;
        for(let i=0; i< brZadatakaLength; i++){
            if(zadaci[i]<0 || zadaci[i]>10){
               if(greska!=0){
                   tekst+=",";
               }
               tekst= tekst + "z" + i;
               greska=13;
            }
        }
        if(brZadatakaLength!=brVjezbi){
            if(greska!=0){
                 tekst+=",";
            }   
            greska=12;
            tekst+="brojZadataka";
        }
        if(greska!=0){
            let obj={
                status: "error",
                data: tekst
            }
            console.log("Parametri nisu prosli provjeru. Objekat: " + obj);
            return;
        }
        /////////////////////////////////////////////////
             let htmlKod = '';
             for (let i=0; i<brVjezbi; i++){
                 htmlKod += '<div class="vjezba" id="divVjezba' + i.toString() + '" onclick="crtajZadatkeKlik(' + i.toString() + ',' + vjezbeObj.brojZadataka[i].toString() + ')">';
                 htmlKod += '<p class="titleVjezbe">Vjezba ' + (i+1).toString() + '</p>';
                 htmlKod += '</div>';
                }
                console.log(htmlKod);
            divDOMelement.innerHTML = htmlKod;    
    }

   
var prethodnaVjezba = null;
var iscrtajZadatke = function(vjezbaDOMelement,brojZadataka){

   if(prethodnaVjezba!=null && prethodnaVjezba != vjezbaDOMelement){
      let children = Array.from(prethodnaVjezba.children);
      children.forEach( el => {
          if(el!=children[0])
          el.style.display = "none";
      });
   }
       prethodnaVjezba = vjezbaDOMelement;
   
        
      let id = vjezbaDOMelement.id;
      console.log("id prije parse: " + id);
      id = parseInt(id.replace('divVjezba', ''));
      console.log("id poslije parse: " + id);
      let htmlKod = '';
    // console.log("prethodni.id= "+prethodni.id + ", vjezveDOM.id:"+ vjezbaDOMelement.id);
     //prethodni=vjezbaDOMelement;

    if(!vjezbaDOMelement.innerHTML.toString().includes('prikazaniZadaci') || 
       !vjezbaDOMelement.innerHTML.toString().includes('display: block') ){

          htmlKod += '<p class = "naslov">Vjezba ' + (id+1).toString() + '</p>';
          htmlKod += '<div class="prikazaniZadaci" style="display: block">';

          for(let i=0; i<brojZadataka; i++){
            htmlKod += '<div class="zadatak">';
            htmlKod += '<p>ZADATAK ' + (i+1).toString() + '</p>';
            htmlKod += '</div>';
          }
          htmlKod += '</div>';
          vjezbaDOMelement.innerHTML = htmlKod;
          console.log("innerhtml:" + vjezbaDOMelement.innerHTML);
     }
      else{

          console.log("else");

          htmlKod += '<p class = "naslov">Vjezba ' + (id+1).toString() + '</p>';
          htmlKod += '<div class="prikazaniZadaci" style="display: none">';

          for(let i=0; i<brojZadataka; i++){
            htmlKod += '<div class="zadatak">';
            htmlKod += '<p>ZADATAK ' + (i+1).toString() + '</p>';
            htmlKod += '</div>';
          }
          htmlKod += '</div>';
          vjezbaDOMelement.innerHTML = htmlKod;
          console.log("innerhtml:" + vjezbaDOMelement.innerHTML);
          
      }
       
    }


    return {
        dodajInputPolja: dodajInputPolja,
        posaljiPodatke: posaljiPodatke,
        dohvatiPodatke: dohvatiPodatke,
        iscrtajVjezbe: iscrtajVjezbe,
        iscrtajZadatke: iscrtajZadatke
      };
    })();