let assert = chai.assert;
//let testoviParser = require('../TestoviParser');
describe("TestoviParser", function () {
  describe("porediRezultate()", function () {
    it("rezultat1 i rezultat2 imaju iste fullTitle-ove", function () {
      let objTacnost1 = {
        "stats": {
          "suites": 2,
          "tests": 2,
          "passes": 2,
          "pending": 0,
          "failures": 0,
          "start": "2021-11-05T15:00:26.343Z",
          "end": "2021-11-05T15:00:26.352Z",
          "duration": 9,
        },
        "tests": [
          {
            "title": "should draw 3 rows when parameter are 2,3",
            "fullTitle":
              "Tabela crtaj() should draw 3 rows when parameter are 2,3",
            "file": null,
            "duration": 1,
            "currentRetry": 0,
            "speed": "fast",
            "err": {},
          },
          {
            "title": "should draw 2 columns in row 2 when parameter are 2,3",
            "fullTitle":
              "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
            "file": null,
            "duration": 0,
            "currentRetry": 0,
            "speed": "fast",
            "err": {},
          }
        ],
        "pending": [],
        "failures": [],
        "passes": [
          {
            "title": "should draw 3 rows when parameter are 2,3",
            "fullTitle":
              "Tabela crtaj() should draw 3 rows when parameter are 2,3",
            "file": null,
            "duration": 1,
            "currentRetry": 0,
            "speed": "fast",
            "err": {},
          },
          {
            "title": "should draw 2 columns in row 2 when parameter are 2,3",
            "fullTitle":
              "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
            "file": null,
            "duration": 0,
            "currentRetry": 0,
            "speed": "fast",
            "err": {},
          }
        ],
      };

      let objTacnost2 = {
        "stats": {
          "suites": 2,
          "tests": 2,
          "passes": 2,
          "pending": 0,
          "failures": 0,
          "start": "2021-11-05T15:00:26.343Z",
          "end": "2021-11-05T15:00:26.352Z",
          "duration": 9,
        },
        "tests": [
          {
            "title": "should draw 3 rows when parameter are 2,3",
            "fullTitle":
              "Tabela crtaj() should draw 3 rows when parameter are 2,3",
            "file": null,
            "duration": 1,
            "currentRetry": 0,
            "speed": "fast",
            "err": {},
          },
          {
            "title": "should draw 2 columns in row 2 when parameter are 2,3",
            "fullTitle":
              "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
            "file": null,
            "duration": 0,
            "currentRetry": 0,
            "speed": "fast",
            "err": {},
          }
        ],
        "pending": [],
        "failures": [],
        "passes": [
          {
            "title": "should draw 3 rows when parameter are 2,3",
            "fullTitle":
              "Tabela crtaj() should draw 3 rows when parameter are 2,3",
            "file": null,
            "duration": 1,
            "currentRetry": 0,
            "speed": "fast",
            "err": {},
          },
          {
            "title": "should draw 2 columns in row 2 when parameter are 2,3",
            "fullTitle":
              "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
            "file": null,
            "duration": 0,
            "currentRetry": 0,
            "speed": "fast",
            "err": {},
          }
        ],
      };

      let str1 = JSON.stringify(objTacnost1),
        str2 = JSON.stringify(objTacnost2);
      let rezultatPoredjenja = TestoviParser.porediRezultate(str1, str2);
      assert.equal(rezultatPoredjenja.promjena, "100%");
      var expect = chai.expect;

      expect(rezultatPoredjenja.greske).to.deep.equal([]);
    });

    //test2
    it("rezultat1 i rezultat2 imaju iste fullTitle-ove ali rezultat1 ima 1 fail a rezultat2 0 failova", function () {
      let objTacnost1 = {
        "stats": {
          "suites": 2,
          "tests": 2,
          "passes": 1,
          "pending": 0,
          "failures": 1,
          "start": "2021-11-05T15:00:26.343Z",
          "end": "2021-11-05T15:00:26.352Z",
          "duration": 9,
        },
        "tests": [
          {
            "title": "T1",
            "fullTitle": "TEST1",
            "file": null,
            "duration": 1,
            "currentRetry": 0,
            "speed": "fast",
            "err": {},
          },
          {
            "title": "T2",
            "fullTitle": "TEST2",
            "file": null,
            "duration": 0,
            "currentRetry": 0,
            "speed": "fast",
            "err": {},
          }
        ],
        "pending": [],
        "failures": [
          {
            "title": "T1",
            "fullTitle": "TEST1",
            "file": null,
            "duration": 1,
            "currentRetry": 0,
            "speed": "fast",
            "err": {},
          }
        ],
        "passes": [
          {
            "title": "T2",
            "fullTitle": "TEST2",
            "file": null,
            "duration": 0,
            "currentRetry": 0,
            "speed": "fast",
            "err": {},
          }
        ],
      };

      let objTacnost2 = {
        "stats": {
          "suites": 2,
          "tests": 2,
          "passes": 2,
          "pending": 0,
          "failures": 0,
          "start": "2021-11-05T15:00:26.343Z",
          "end": "2021-11-05T15:00:26.352Z",
          "duration": 9,
        },
        "tests": [
          {
            "title": "T1",
            "fullTitle": "TEST1",
            "file": null,
            "duration": 1,
            "currentRetry": 0,
            "speed": "fast",
            "err": {},
          },
          {
            "title": "T2",
            "fullTitle": "TEST2",
            "file": null,
            "duration": 0,
            "currentRetry": 0,
            "speed": "fast",
            "err": {},
          }
        ],
        "pending": [],
        "failures": [],
        "passes": [
          {
            "title": "T1",
            "fullTitle": "TEST1",
            "file": null,
            "duration": 1,
            "currentRetry": 0,
            "speed": "fast",
            "err": {},
          },
          {
            "title": "T2",
            "fullTitle": "TEST2",
            "file": null,
            "duration": 0,
            "currentRetry": 0,
            "speed": "fast",
            "err": {},
          }
        ]
      };

      let str1 = JSON.stringify(objTacnost1),
        str2 = JSON.stringify(objTacnost2);
      let rezultatPoredjenja = TestoviParser.porediRezultate(str1, str2);
      assert.equal(rezultatPoredjenja.promjena, "100%");
      var expect = chai.expect;

      expect(rezultatPoredjenja.greske).to.deep.equal([]);
    });

    //test 3
    it("rezultat1 i rezultat2 imaju iste fullTitle-ove i rezultat2 ima jedan pokvaren test u odnosu na rezultat1", function () {
      let objTacnost1 = {
        "stats": {
          "suites": 2,
          "tests": 2,
          "passes": 2,
          "pending": 0,
          "failures": 0,
          "start": "2021-11-05T15:00:26.343Z",
          "end": "2021-11-05T15:00:26.352Z",
          "duration": 9,
        },
        "tests": [
            {
                "title": "T1",
                "fullTitle": "TEST1",
                "file": null,
                "duration": 1,
                "currentRetry": 0,
                "speed": "fast",
                "err": {},
              },
              {
                "title": "T2",
                "fullTitle": "TEST2",
                "file": null,
                "duration": 1,
                "currentRetry": 0,
                "speed": "fast",
                "err": {},
              }
        ],
        "pending": [],
        "failures": [],
        "passes": [
          {
            "title": "T1",
            "fullTitle": "TEST1",
            "file": null,
            "duration": 1,
            "currentRetry": 0,
            "speed": "fast",
            "err": {},
          },
          {
            "title": "T2",
            "fullTitle": "TEST2",
            "file": null,
            "duration": 0,
            "currentRetry": 0,
            "speed": "fast",
            "err": {},
          }
        ],
      };

      let objTacnost2 = {
        "stats": {
          "suites": 2,
          "tests": 2,
          "passes": 1,
          "pending": 0,
          "failures": 1,
          "start": "2021-11-05T15:00:26.343Z",
          "end": "2021-11-05T15:00:26.352Z",
          "duration": 9,
        },
        "tests": [
            {
                "title": "T1",
                "fullTitle": "TEST1",
                "file": null,
                "duration": 1,
                "currentRetry": 0,
                "speed": "fast",
                "err": {},
              },
              {
                "title": "T2",
                "fullTitle": "TEST2",
                "file": null,
                "duration": 0,
                "currentRetry": 0,
                "speed": "fast",
                "err": {},
              }
        ],
        "pending": [],
        "failures": [
            {
                "title": "T1",
                "fullTitle": "TEST1",
                "file": null,
                "duration": 1,
                "currentRetry": 0,
                "speed": "fast",
                "err": {},
              }
        ],
        "passes": [
            {
                "title": "T2",
                "fullTitle": "TEST2",
                "file": null,
                "duration": 0,
                "currentRetry": 0,
                "speed": "fast",
                "err": {},
              }
        ]
      };

      let str1 = JSON.stringify(objTacnost1),
        str2 = JSON.stringify(objTacnost2);
      let rezultatPoredjenja = TestoviParser.porediRezultate(str1, str2);
      assert.equal(rezultatPoredjenja.promjena, "50%");
      var expect = chai.expect;

      expect(rezultatPoredjenja.greske).to.deep.equal(["TEST1"]);
    });

    //test 4
    it("rezultat1 i rezultat2 imaju iste fullTitle-ove i rezultat2 - test leksikografskog sortiranja", function () {
      let objTacnost1 = {
        "stats": {
          "suites": 2,
          "tests": 3,
          "passes": 2,
          "pending": 0,
          "failures": 1,
          "start": "2021-11-05T15:00:26.343Z",
          "end": "2021-11-05T15:00:26.352Z",
          "duration": 9,
        },
        "tests": [
            {
                "title": "T1",
                "fullTitle": "TEST1",
                "file": null,
                "duration": 1,
                "currentRetry": 0,
                "speed": "fast",
                "err": {},
              },
              {
                "title": "T2",
                "fullTitle": "TEST2",
                "file": null,
                "duration": 0,
                "currentRetry": 0,
                "speed": "fast",
                "err": {},
              },
              {
                "title": "T3",
                "fullTitle": "TEST3",
                "file": null,
                "duration": 1,
                "currentRetry": 0,
                "speed": "fast",
                "err": {},
              }
        ],
        "pending": [],
        "failures": [
            {
                "title": "T3",
                "fullTitle": "TEST3",
                "file": null,
                "duration": 1,
                "currentRetry": 0,
                "speed": "fast",
                "err": {}
                }
        ],
        "passes": [
            {
                "title": "T1",
                "fullTitle": "TEST1",
                "file": null,
                "duration": 1,
                "currentRetry": 0,
                "speed": "fast",
                "err": {},
              },
              {
                "title": "T2",
                "fullTitle": "TEST2",
                "file": null,
                "duration": 0,
                "currentRetry": 0,
                "speed": "fast",
                "err": {},
              }
        ]
      };

      let objTacnost2 = {
        "stats": {
          "suites": 2,
          "tests": 3,
          "passes": 1,
          "pending": 0,
          "failures": 2,
          "start": "2021-11-05T15:00:26.343Z",
          "end": "2021-11-05T15:00:26.352Z",
          "duration": 9,
        },
        "tests": [
            {
                "title": "T1",
                "fullTitle": "TEST1",
                "file": null,
                "duration": 1,
                "currentRetry": 0,
                "speed": "fast",
                "err": {},
              },
              {
                "title": "T2",
                "fullTitle": "TEST2",
                "file": null,
                "duration": 0,
                "currentRetry": 0,
                "speed": "fast",
                "err": {},
              },
          {
            "title": "T3",
            "fullTitle": "TEST3",
            "file": null,
            "duration": 1,
            "currentRetry": 0,
            "speed": "fast",
            "err": {}
            }
        ],
        "pending": [],
        "failures": [
            {
                "title": "T3",
                "fullTitle": "TEST3",
                "file": null,
                "duration": 1,
                "currentRetry": 0,
                "speed": "fast",
                "err": {},
              },
              {
                "title": "T1",
                "fullTitle": "TEST1",
                "file": null,
                "duration": 1,
                "currentRetry": 0,
                "speed": "fast",
                "err": {},
              }
        ],
        "passes": [
            {
                "title": "T2",
                "fullTitle": "TEST2",
                "file": null,
                "duration": 0,
                "currentRetry": 0,
                "speed": "fast",
                "err": {},
              }
        ]
      };

      let str1 = JSON.stringify(objTacnost1),
        str2 = JSON.stringify(objTacnost2);
      let rezultatPoredjenja = TestoviParser.porediRezultate(str1, str2);
      assert.equal(rezultatPoredjenja.promjena, "33.3%");
      var expect = chai.expect;

      expect(rezultatPoredjenja.greske).to.deep.equal(["TEST1", "TEST3"]);
    });

    //razliciti testovi u r1 i r2
      //test 5
      it("razliciti testovi", function () {
        let objTacnost1 = {
          "stats": {
            "suites": 2,
            "tests": 3,
            "passes": 1,
            "pending": 0,
            "failures": 2,
            "start": "2021-11-05T15:00:26.343Z",
            "end": "2021-11-05T15:00:26.352Z",
            "duration": 9,
          },
          "tests": [
            {
                "title": "T1",
                "fullTitle": "TEST1",
                "file": null,
                "duration": 1,
                "currentRetry": 0,
                "speed": "fast",
                "err": {},
              },
              {
                "title": "T2",
                "fullTitle": "TEST2",
                "file": null,
                "duration": 0,
                "currentRetry": 0,
                "speed": "fast",
                "err": {},
              },
              {
                "title": "T3",
                "fullTitle": "TEST3",
                "file": null,
                "duration": 1,
                "currentRetry": 0,
                "speed": "fast",
                "err": {},
              }
          ],
          "pending": [],
          "failures": [
            {
                "title": "T1",
                "fullTitle": "TEST1",
                "file": null,
                "duration": 1,
                "currentRetry": 0,
                "speed": "fast",
                "err": {}
                },
              {
                  "title": "T3",
                  "fullTitle": "TEST3",
                  "file": null,
                  "duration": 1,
                  "currentRetry": 0,
                  "speed": "fast",
                  "err": {}
                  }
          ],
          "passes": [
            {
              "title": "T2",
              "fullTitle": "TEST2",
              "file": null,
              "duration": 0,
              "currentRetry": 0,
              "speed": "fast",
              "err": {},
            }
          ]
        };
  
        let objTacnost2 = {
          "stats": {
            "suites": 2,
            "tests": 3,
            "passes": 2,
            "pending": 0,
            "failures": 1,
            "start": "2021-11-05T15:00:26.343Z",
            "end": "2021-11-05T15:00:26.352Z",
            "duration": 9,
          },
          "tests": [
            {
                "title": "T1",
                "fullTitle": "TEST1",
                "file": null,
                "duration": 1,
                "currentRetry": 0,
                "speed": "fast",
                "err": {},
              },
              {
                "title": "T2",
                "fullTitle": "TEST2",
                "file": null,
                "duration": 1,
                "currentRetry": 0,
                "speed": "fast",
                "err": {},
              },
            {
              "title": "T4",
              "fullTitle": "TEST4",
              "file": null,
              "duration": 1,
              "currentRetry": 0,
              "speed": "fast",
              "err": {}
              }
          ],
          "pending": [],
          "failures": [
            {
                "title": "T2",
                "fullTitle": "TEST2",
                "file": null,
                "duration": 1,
                "currentRetry": 0,
                "speed": "fast",
                "err": {},
              }
          ],
          "passes": [
            {
                "title": "T1",
                "fullTitle": "TEST1",
                "file": null,
                "duration": 0,
                "currentRetry": 0,
                "speed": "fast",
                "err": {},
              },
              {
                "title": "T4",
                "fullTitle": "TEST4",
                "file": null,
                "duration": 0,
                "currentRetry": 0,
                "speed": "fast",
                "err": {},
              }
          ]
        };
  
        let str1 = JSON.stringify(objTacnost1),
          str2 = JSON.stringify(objTacnost2);
        let rezultatPoredjenja = TestoviParser.porediRezultate(str1, str2);
        assert.equal(rezultatPoredjenja.promjena, "50%");
        var expect = chai.expect;
  
        expect(rezultatPoredjenja.greske).to.deep.equal(["TEST3", "TEST2"]);
      });

           //test 6
           it("test slucaj nule u nazivniku", function () {
            let objTacnost1 = {
              "stats": {
                "suites": 2,
                "tests": 1,
                "passes": 1,
                "pending": 0,
                "failures": 2,
                "start": "2021-11-05T15:00:26.343Z",
                "end": "2021-11-05T15:00:26.352Z",
                "duration": 9,
              },
              "tests": [ {
                "title": "T1",
                "fullTitle": "TEST1",
                "file": null,
                "duration": 0,
                "currentRetry": 0,
                "speed": "fast",
                "err": {},
              }
              ],
              "pending": [],
              "failures": [
              ],
              "passes": [ {
                "title": "T1",
                "fullTitle": "TEST1",
                "file": null,
                "duration": 0,
                "currentRetry": 0,
                "speed": "fast",
                "err": {},
              }
              ]
            };
      
            let objTacnost2 = {
              "stats": {
                "suites": 2,
                "tests": 0,
                "passes": 0,
                "pending": 0,
                "failures": 0,
                "start": "2021-11-05T15:00:26.343Z",
                "end": "2021-11-05T15:00:26.352Z",
                "duration": 9,
              },
              "tests": [],
              "pending": [],
              "failures": [
              ],
              "passes": [
              ]
            };
      
            let str1 = JSON.stringify(objTacnost1),
              str2 = JSON.stringify(objTacnost2);
            let rezultatPoredjenja = TestoviParser.porediRezultate(str1, str2);
            assert.equal(rezultatPoredjenja.promjena, "0%");
            var expect = chai.expect;
      
            expect(rezultatPoredjenja.greske).to.deep.equal(["Testovi se ne mogu izvršiti"]);
          });

           //test 7
           it("test slucaj nule u nazivniku", function () {
            let objTacnost1 = {
              "stats": {
                "suites": 2,
                "tests": 0,
                "passes": 0,
                "pending": 0,
                "failures": 2,
                "start": "2021-11-05T15:00:26.343Z",
                "end": "2021-11-05T15:00:26.352Z",
                "duration": 9,
              },
              "tests": [],
              "pending": [],
              "failures": [
              ],
              "passes": []
            };
      
            let objTacnost2 = {
              "stats": {
                "suites": 2,
                "tests": 0,
                "passes": 0,
                "pending": 0,
                "failures": 0,
                "start": "2021-11-05T15:00:26.343Z",
                "end": "2021-11-05T15:00:26.352Z",
                "duration": 9,
              },
              "tests": [],
              "pending": [],
              "failures": [
              ],
              "passes": [
              ]
            };
      
            let str1 = JSON.stringify(objTacnost1),
              str2 = JSON.stringify(objTacnost2);
            let rezultatPoredjenja = TestoviParser.porediRezultate(str1, str2);
            assert.equal(rezultatPoredjenja.promjena, "0%");
            var expect = chai.expect;
      
            expect(rezultatPoredjenja.greske).to.deep.equal(["Testovi se ne mogu izvršiti"]);
          });

  });
});
