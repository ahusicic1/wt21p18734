let assert = chai.assert;
//let testoviParser = require('../TestoviParser'); 
describe('TestoviParser', function () {
  describe('dajTacnost()', function () {
    //test 1
    it("should return 0% when string is not valid", function () {
      let objTacnost = ({ "asdfghj": "qwertzu" });
 
      let str = JSON.stringify(objTacnost);
      let tacnostRezultat = TestoviParser.dajTacnost(str);
      assert.equal(tacnostRezultat.tacnost, "0%");
      var expect = chai.expect;
  
      expect(tacnostRezultat.greske).to.deep.equal(["Testovi se ne mogu izvršiti"]);
    });
    //test 2
    it("should return 0% when string is not a valid json string", function () {
      let str = "abc123"
      let tacnostRezultat = TestoviParser.dajTacnost(str);
      assert.equal(tacnostRezultat.tacnost, "0%");
      var expect = chai.expect;
  
      expect(tacnostRezultat.greske).to.deep.equal(["Testovi se ne mogu izvršiti"]);
    });
    //test 3
    it("should return 0% when all tests failed", function () {
      let objTacnost = ({
        "stats": {
        "suites": 2,
        "tests": 2,
        "passes": 0,  
        "pending": 0,
        "failures": 2,
        "start": "2021-11-05T15:00:26.343Z",
        "end": "2021-11-05T15:00:26.352Z",
        "duration": 9
        },
        "tests": [
        {
        "title": "should draw 3 rows when parameter are 2,3",
        "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
        "file": null,
        "duration": 1,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
        },
        {
        "title": "should draw 2 columns in row 2 when parameter are 2,3",
        "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
        }
        ],
        "pending": [],
        "failures": [ {
          "title": "should draw 3 rows when parameter are 2,3",
          "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
          "file": null,
          "duration": 1,
          "currentRetry": 0,
          "speed": "fast",
          "err": {}
          },
          {
          "title": "should draw 2 columns in row 2 when parameter are 2,3",
          "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
          "file": null,
          "duration": 0,
          "currentRetry": 0,
          "speed": "fast",
          "err": {}
          }],
        "passes": []
         });
 
      let str = JSON.stringify(objTacnost);
      let tacnostRezultat = TestoviParser.dajTacnost(str);
      assert.equal(tacnostRezultat.tacnost, "0%");
      var expect = chai.expect;
  
      expect(tacnostRezultat.greske).to.deep.equal(["Tabela crtaj() should draw 3 rows when parameter are 2,3", "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3" ]);
    });
//test 4
it("should return 0% when all tests failed (another example)", function () {
  let objTacnost = (
    {
      "stats": {
        "suites": 2,
        "tests": 3,
        "passes": 0,
        "pending": 0,
        "failures": 3,
        "start": "2021-11-24T09:25:07.575Z",
        "end": "2021-11-24T09:25:07.590Z",
        "duration": 15
      },
      "tests": [
        {
          "title": "false assertions",
          "fullTitle": "TestoviParser dajTacnost() false assertions",
          "file": null,
          "duration": 3,
          "currentRetry": 0,
          "err": {
            "message": "expected '0%' to equal '70%'",
            "showDiff": true,
            "actual": "0%",
            "expected": "70%",
            "operator": "strictEqual",
            "stack": "AssertionError@https://unpkg.com/chai/chai.js:9574:13\n[3]</module.exports/Assertion.prototype.assert@https://unpkg.com/chai/chai.js:250:13\n[6]</module.exports/assert.equal@https://unpkg.com/chai/chai.js:4365:10\n@http://127.0.0.1:5500/test/test.js:12:14\n"
          }
        },
        {
          "title": "false assertions",
          "fullTitle": "TestoviParser dajTacnost() false assertions",
          "file": null,
          "duration": 1,
          "currentRetry": 0,
          "err": {
            "message": "expected '0%' to equal '10%'",
            "showDiff": true,
            "actual": "0%",
            "expected": "10%",
            "operator": "strictEqual",
            "stack": "AssertionError@https://unpkg.com/chai/chai.js:9574:13\n[3]</module.exports/Assertion.prototype.assert@https://unpkg.com/chai/chai.js:250:13\n[6]</module.exports/assert.equal@https://unpkg.com/chai/chai.js:4365:10\n@http://127.0.0.1:5500/test/test.js:73:14\n"
          }
        },
        {
          "title": "false assertions",
          "fullTitle": "TestoviParser dajTacnost() false assertions",
          "file": null,
          "duration": 0,
          "currentRetry": 0,
          "err": {
            "message": "expected '100%' to equal '101%'",
            "showDiff": true,
            "actual": "100%",
            "expected": "101%",
            "operator": "strictEqual",
            "stack": "AssertionError@https://unpkg.com/chai/chai.js:9574:13\n[3]</module.exports/Assertion.prototype.assert@https://unpkg.com/chai/chai.js:250:13\n[6]</module.exports/assert.equal@https://unpkg.com/chai/chai.js:4365:10\n@http://127.0.0.1:5500/test/test.js:138:14\n"
          }
        }
      ],
      "pending": [],
      "failures": [
        {
          "title": "false assertions",
          "fullTitle": "TestoviParser dajTacnost() false assertions",
          "file": null,
          "duration": 3,
          "currentRetry": 0,
          "err": {
            "message": "expected '0%' to equal '70%'",
            "showDiff": true,
            "actual": "0%",
            "expected": "70%",
            "operator": "strictEqual",
            "stack": "AssertionError@https://unpkg.com/chai/chai.js:9574:13\n[3]</module.exports/Assertion.prototype.assert@https://unpkg.com/chai/chai.js:250:13\n[6]</module.exports/assert.equal@https://unpkg.com/chai/chai.js:4365:10\n@http://127.0.0.1:5500/test/test.js:12:14\n"
          }
        },
        {
          "title": "false assertions",
          "fullTitle": "TestoviParser dajTacnost() false assertions",
          "file": null,
          "duration": 1,
          "currentRetry": 0,
          "err": {
            "message": "expected '0%' to equal '10%'",
            "showDiff": true,
            "actual": "0%",
            "expected": "10%",
            "operator": "strictEqual",
            "stack": "AssertionError@https://unpkg.com/chai/chai.js:9574:13\n[3]</module.exports/Assertion.prototype.assert@https://unpkg.com/chai/chai.js:250:13\n[6]</module.exports/assert.equal@https://unpkg.com/chai/chai.js:4365:10\n@http://127.0.0.1:5500/test/test.js:73:14\n"
          }
        },
        {
          "title": "false assertions",
          "fullTitle": "TestoviParser dajTacnost() false assertions",
          "file": null,
          "duration": 0,
          "currentRetry": 0,
          "err": {
            "message": "expected '100%' to equal '101%'",
            "showDiff": true,
            "actual": "100%",
            "expected": "101%",
            "operator": "strictEqual",
            "stack": "AssertionError@https://unpkg.com/chai/chai.js:9574:13\n[3]</module.exports/Assertion.prototype.assert@https://unpkg.com/chai/chai.js:250:13\n[6]</module.exports/assert.equal@https://unpkg.com/chai/chai.js:4365:10\n@http://127.0.0.1:5500/test/test.js:138:14\n"
          }
        }
      ],
      "passes": []
    }
  );

  let str = JSON.stringify(objTacnost);
  let tacnostRezultat = TestoviParser.dajTacnost(str);
  assert.equal(tacnostRezultat.tacnost, "0%");
  var expect = chai.expect;

  expect(tacnostRezultat.greske).to.deep.equal(["TestoviParser dajTacnost() false assertions", "TestoviParser dajTacnost() false assertions", "TestoviParser dajTacnost() false assertions" ]);
});
    //test 5
    it("should return 100% when all tests passed", function () {
      let objTacnost = ( {
        "stats": {
        "suites": 2,
        "tests": 2,
        "passes": 2,
        "pending": 0,
        "failures": 0,
        "start": "2021-11-05T15:00:26.343Z",
        "end": "2021-11-05T15:00:26.352Z",
        "duration": 9
        },
        "tests": [
        {
        "title": "should draw 3 rows when parameter are 2,3",
        "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
        "file": null,
        "duration": 1,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
        },
        {
        "title": "should draw 2 columns in row 2 when parameter are 2,3",
        "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
        }
        ],
        "pending": [],
        "failures": [],
        "passes": [
        {
        "title": "should draw 3 rows when parameter are 2,3",
        "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
        "file": null,
        "duration": 1,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
        },
        {
        "title": "should draw 2 columns in row 2 when parameter are 2,3",
        "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
        "file": null,
        "duration": 0,
        "currentRetry": 0,
        "speed": "fast",
        "err": {}
        }
        ]
      });
 
      let str = JSON.stringify(objTacnost);
      let tacnostRezultat = TestoviParser.dajTacnost(str);
      assert.equal(tacnostRezultat.tacnost, "100%");
      var expect = chai.expect;
      expect(tacnostRezultat.greske).to.deep.equal([]);
    });
       //test 6
       it("should return 100% when all tests passed (another example)", function () {
        let objTacnost = ( 
          {
            "stats": {
              "suites": 2,
              "tests": 3,
              "passes": 3,
              "pending": 0,
              "failures": 0,
              "start": "2021-11-24T09:20:15.584Z",
              "end": "2021-11-24T09:20:15.609Z",
              "duration": 25
            },
            "tests": [
              {
                "title": "should return zero when string is not valid",
                "fullTitle": "TestoviParser dajTacnost() should return zero when string is not valid",
                "file": null,
                "duration": 1,
                "currentRetry": 0,
                "speed": "fast",
                "err": {}
              },
              {
                "title": "should return zero when number of passed tests is 0",
                "fullTitle": "TestoviParser dajTacnost() should return zero when number of passed tests is 0",
                "file": null,
                "duration": 4,
                "currentRetry": 0,
                "speed": "fast",
                "err": {}
              },
              {
                "title": "should return 100% when all tests passed",
                "fullTitle": "TestoviParser dajTacnost() should return 100% when all tests passed",
                "file": null,
                "duration": 1,
                "currentRetry": 0,
                "speed": "fast",
                "err": {}
              }
            ],
            "pending": [],
            "failures": [],
            "passes": [
              {
                "title": "should return zero when string is not valid",
                "fullTitle": "TestoviParser dajTacnost() should return zero when string is not valid",
                "file": null,
                "duration": 1,
                "currentRetry": 0,
                "speed": "fast",
                "err": {}
              },
              {
                "title": "should return zero when number of passed tests is 0",
                "fullTitle": "TestoviParser dajTacnost() should return zero when number of passed tests is 0",
                "file": null,
                "duration": 4,
                "currentRetry": 0,
                "speed": "fast",
                "err": {}
              },
              {
                "title": "should return 100% when all tests passed",
                "fullTitle": "TestoviParser dajTacnost() should return 100% when all tests passed",
                "file": null,
                "duration": 1,
                "currentRetry": 0,
                "speed": "fast",
                "err": {}
              }
            ]
          }
        );
   
        let str = JSON.stringify(objTacnost);
        let tacnostRezultat = TestoviParser.dajTacnost(str);
        assert.equal(tacnostRezultat.tacnost, "100%");
        var expect = chai.expect;
        expect(tacnostRezultat.greske).to.deep.equal([]);
      });
    //test 7
    it("should return 33.3% when: passed tests = 1 and failed tests = 2 ", function () {
      let objTacnost = ({
        "stats": {
          "suites": 2,
          "tests": 3,
          "passes": 1,
          "pending": 0,
          "failures": 2,
          "start": "2021-11-24T12:11:03.245Z",
          "end": "2021-11-24T12:11:03.257Z",
          "duration": 12
        },
        "tests": [
          {
            "title": "should return zero when string is not valid",
            "fullTitle": "TestoviParser dajTacnost() should return zero when string is not valid",
            "file": null,
            "duration": 0,
            "currentRetry": 0,
            "speed": "fast",
            "err": {}
          },
          {
            "title": "should fail",
            "fullTitle": "TestoviParser dajTacnost() should fail",
            "file": null,
            "duration": 2,
            "currentRetry": 0,
            "err": {
              "message": "expected '0%' to equal '70%'",
              "showDiff": true,
              "actual": "0%",
              "expected": "70%",
              "operator": "strictEqual",
              "stack": "AssertionError@https://unpkg.com/chai/chai.js:9574:13\n[3]</module.exports/Assertion.prototype.assert@https://unpkg.com/chai/chai.js:250:13\n[6]</module.exports/assert.equal@https://unpkg.com/chai/chai.js:4365:10\n@http://127.0.0.1:5500/test/test.js:72:14\n"
            }
          },
          {
            "title": "should fail",
            "fullTitle": "TestoviParser dajTacnost() should fail",
            "file": null,
            "duration": 0,
            "currentRetry": 0,
            "err": {
              "message": "expected '100%' to equal '101%'",
              "showDiff": true,
              "actual": "100%",
              "expected": "101%",
              "operator": "strictEqual",
              "stack": "AssertionError@https://unpkg.com/chai/chai.js:9574:13\n[3]</module.exports/Assertion.prototype.assert@https://unpkg.com/chai/chai.js:250:13\n[6]</module.exports/assert.equal@https://unpkg.com/chai/chai.js:4365:10\n@http://127.0.0.1:5500/test/test.js:137:14\n"
            }
          }
        ],
        "pending": [],
        "failures": [
          {
            "title": "should fail",
            "fullTitle": "TestoviParser dajTacnost() should fail",
            "file": null,
            "duration": 2,
            "currentRetry": 0,
            "err": {
              "message": "expected '0%' to equal '70%'",
              "showDiff": true,
              "actual": "0%",
              "expected": "70%",
              "operator": "strictEqual",
              "stack": "AssertionError@https://unpkg.com/chai/chai.js:9574:13\n[3]</module.exports/Assertion.prototype.assert@https://unpkg.com/chai/chai.js:250:13\n[6]</module.exports/assert.equal@https://unpkg.com/chai/chai.js:4365:10\n@http://127.0.0.1:5500/test/test.js:72:14\n"
            }
          },
          {
            "title": "should fail",
            "fullTitle": "TestoviParser dajTacnost() should fail",
            "file": null,
            "duration": 0,
            "currentRetry": 0,
            "err": {
              "message": "expected '100%' to equal '101%'",
              "showDiff": true,
              "actual": "100%",
              "expected": "101%",
              "operator": "strictEqual",
              "stack": "AssertionError@https://unpkg.com/chai/chai.js:9574:13\n[3]</module.exports/Assertion.prototype.assert@https://unpkg.com/chai/chai.js:250:13\n[6]</module.exports/assert.equal@https://unpkg.com/chai/chai.js:4365:10\n@http://127.0.0.1:5500/test/test.js:137:14\n"
            }
          }
        ],
        "passes": [
          {
            "title": "should return zero when string is not valid",
            "fullTitle": "TestoviParser dajTacnost() should return zero when string is not valid",
            "file": null,
            "duration": 0,
            "currentRetry": 0,
            "speed": "fast",
            "err": {}
          }
        ]
      }
        
         );
 
      let str = JSON.stringify(objTacnost);
      let tacnostRezultat = TestoviParser.dajTacnost(str);
      assert.equal(tacnostRezultat.tacnost, "33.3%");
      var expect = chai.expect;
  
      expect(tacnostRezultat.greske).to.deep.equal(["TestoviParser dajTacnost() should fail", "TestoviParser dajTacnost() should fail" ]);
    });
     //test 8
     it("should return 66.7% when: passed tests = 2 and failed tests = 1 ", function () {
      let objTacnost = ({
        "stats": {
          "suites": 2,
          "tests": 3,
          "passes": 2,
          "pending": 0,
          "failures": 1,
          "start": "2021-11-24T09:00:51.869Z",
          "end": "2021-11-24T09:00:51.889Z",
          "duration": 20
        },
        "tests": [
          {
            "title": "should return zero when string is not valid",
            "fullTitle": "TestoviParser dajTacnost() should return zero when string is not valid",
            "file": null,
            "duration": 1,
            "currentRetry": 0,
            "speed": "fast",
            "err": {}
          },
          {
            "title": "should return zero when number of passed tests is 0",
            "fullTitle": "TestoviParser dajTacnost() should return zero when number of passed tests is 0",
            "file": null,
            "duration": 2,
            "currentRetry": 0,
            "speed": "fast",
            "err": {}
          },
          {
            "title": "should return 100% when all tests passed",
            "fullTitle": "TestoviParser dajTacnost() should return 100% when all tests passed",
            "file": null,
            "duration": 2,
            "currentRetry": 0,
            "err": {
              "message": "expected [] to equal []",
              "showDiff": true,
              "actual": "[]",
              "expected": "[]",
              "operator": "deepStrictEqual",
              "stack": "AssertionError@https://unpkg.com/chai/chai.js:9574:13\n[3]</module.exports/Assertion.prototype.assert@https://unpkg.com/chai/chai.js:250:13\n[6]</module.exports/assert.equal@https://unpkg.com/chai/chai.js:4365:10\n@http://127.0.0.1:5500/test/test.js:136:14\n"
            }
          }
        ],
        "pending": [],
        "failures": [
          {
            "title": "should return 100% when all tests passed",
            "fullTitle": "TestoviParser dajTacnost() should return 100% when all tests passed",
            "file": null,
            "duration": 2,
            "currentRetry": 0,
            "err": {
              "message": "expected [] to equal []",
              "showDiff": true,
              "actual": "[]",
              "expected": "[]",
              "operator": "deepStrictEqual",
              "stack": "AssertionError@https://unpkg.com/chai/chai.js:9574:13\n[3]</module.exports/Assertion.prototype.assert@https://unpkg.com/chai/chai.js:250:13\n[6]</module.exports/assert.equal@https://unpkg.com/chai/chai.js:4365:10\n@http://127.0.0.1:5500/test/test.js:136:14\n"
            }
          }
        ],
        "passes": [
          {
            "title": "should return zero when string is not valid",
            "fullTitle": "TestoviParser dajTacnost() should return zero when string is not valid",
            "file": null,
            "duration": 1,
            "currentRetry": 0,
            "speed": "fast",
            "err": {}
          },
          {
            "title": "should return zero when number of passed tests is 0",
            "fullTitle": "TestoviParser dajTacnost() should return zero when number of passed tests is 0",
            "file": null,
            "duration": 2,
            "currentRetry": 0,
            "speed": "fast",
            "err": {}
          }
        ]
      }
         );
 
      let str = JSON.stringify(objTacnost);
      let tacnostRezultat = TestoviParser.dajTacnost(str);
      assert.equal(tacnostRezultat.tacnost, "66.7%");
      var expect = chai.expect;
  
      expect(tacnostRezultat.greske).to.deep.equal(["TestoviParser dajTacnost() should return 100% when all tests passed" ]);
    });
    
  });
});
