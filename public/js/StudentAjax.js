var StudentAjax = (function () {    

    var dodajStudenta = function (student,fnCallback) { 
        var xhttp = new XMLHttpRequest();
    
        xhttp.onreadystatechange = function() {
            console.log("response text je:"+xhttp.responseText);
            var res = JSON.parse(xhttp.response);
            document.getElementById("ajaxstatus").innerHTML = res.status;               
    
      if (xhttp.readyState == 4 && xhttp.responseText.includes("error")) { 
        var xhttpJSON = JSON.parse(xhttp.responseText);

           fnCallback(null, xhttpJSON);
          }
          else if(xhttp.readyState == 4 && xhttp.status == 200){
            var xhttpJSON = JSON.parse(xhttp.responseText);     
            fnCallback(xhttpJSON, null);
          }
          else if(xhttp.readyState == 4 && xhttp.status == 404){
            fnCallback(null, "error");
          }
        };
        console.log("sad ide send");            
        xhttp.open("POST", "http://localhost:3000/student", true);
        xhttp.setRequestHeader("Content-Type", "application/json"); 
        console.log("sad stvarno ide send");   
        console.log("JSONstringify(student) = "+ JSON.stringify(student));
        xhttp.send(JSON.stringify(student));
    };

    var postaviGrupu = function (index, grupa, fnCallback) { 
        var xhttp = new XMLHttpRequest();
        xhttp.open("PUT", "http://localhost:3000/student/" + index.toString() , true);
        xhttp.setRequestHeader("Content-Type", "application/json");
        xhttp.onreadystatechange = function() {
            console.log(xhttp.responseText);
            var res = JSON.parse(xhttp.response);
            document.getElementById("ajaxstatus").innerHTML = res.status;   
          if (xhttp.readyState == 4 && xhttp.status == 200) {
              console.log(xhttp.responseText);
           var xhttpJSON  = xhttp.responseText; //= JSON.parse(xhttp.responseText);
           fnCallback(null, xhttpJSON); 
          }
          else{
            fnCallback(xhttp.response, null);
          }
        };
        console.log("salje se gruoa");
        xhttp.send(JSON.stringify({grupa: grupa}));
    };

    var dodajBatch = function (csvStudenti,fnCallback) { 
        var xhttp = new XMLHttpRequest();
        xhttp.open("POST", "http://localhost:3000/batch/student", true);
        xhttp.setRequestHeader("Content-Type", "text/plain");
        xhttp.onreadystatechange = function() {
            var res = JSON.parse(xhttp.responseText);
            document.getElementById("ajaxstatus").innerHTML = res.status;   
          if (xhttp.readyState == 4 && xhttp.status == 200) {
           console.log("posalji podatke OK");
           var xhttpJSON = JSON.parse(xhttp.responseText);
           fnCallback(null, xhttpJSON);
          }
          else{
          fnCallback(xhttp.response, null);
          }
        };
        xhttp.send(csvStudenti);
    };

    return {
        dodajStudenta: dodajStudenta,
        postaviGrupu: postaviGrupu,
        dodajBatch: dodajBatch
      };
    })();