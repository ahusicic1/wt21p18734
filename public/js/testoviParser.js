var TestoviParser = (function () {
  var procenat = { "tacnost": "0%", "greske": [] }; 
  var jsonObj;
  var dajTacnost = function (jsonString) {
      var ukupnoTestova, uspjesniTestovi, neuspjesniTestovi;
    try {
      jsonObj = JSON.parse(jsonString);
       ukupnoTestova = jsonObj.stats.tests;
       uspjesniTestovi = jsonObj.stats.passes;
       neuspjesniTestovi = jsonObj.stats.failures;
    } catch (err) {
      procenat = { "tacnost": "0%", "greske": ["Testovi se ne mogu izvršiti"] };
      return procenat;
    }
    if(ukupnoTestova>0){
    var postotak = (uspjesniTestovi / ukupnoTestova) * 100; 
    postotak = Math.round((postotak + Number.EPSILON) * 10) / 10;
    }

    var tekstoviGresaka = [];
    jsonObj.failures.forEach((element) => {
      tekstoviGresaka.push(element.fullTitle);
    });

    procenat = { "tacnost": postotak.toString() + "%", "greske": tekstoviGresaka };
    return procenat;
  };

  var porediRezultate = function (rezultat1, rezultat2) {
      try{
    var rezultatFunkcije = {"promjena": "0%", "greske":[]};
    var rezultat1Json = JSON.parse(rezultat1);
    var rezultat2Json = JSON.parse(rezultat2);
    var brojIstih = rezultat1Json.tests.filter(o1 => rezultat2Json.tests.some(o2 => o1.fullTitle == o2.fullTitle));
     if(rezultat1Json["tests"].length == rezultat2Json["tests"].length && brojIstih.length == rezultat1Json["tests"].length){
            // znaci da u rezultatu2 ima isto toliko testova sa istim nazviom ali nema dodatnih jer im je size isti
                 var x = dajTacnost(rezultat2);
                 rezultatFunkcije = {"promjena": x.tacnost, "greske": x.greske.sort()};
                 return rezultatFunkcije;
        }

          //testovi koji padaju u rezultatu1 a nema uopce tih testova u rezultatu2
        var greske1 = rezultat1Json.failures.filter(o1 => 
            rezultat2Json.tests.filter(o2 => o1.fullTitle == o2.fullTitle).length == 0);
        //broj testova koji padaju u r1 a ne pojavljuju se u r2
        var padaju1ne2 = greske1.length;

        var padaju2 = rezultat2Json.failures.length;
        var brojTestova2 = rezultat2Json.stats.tests;

        if( (padaju1ne2 + brojTestova2) == 0 ){
            //moramo vratiti rezultat da ne dodje do dijeljenja nulom
            rezultatFunkcije = {"promjena": "0%", "greske": ["Testovi se ne mogu izvršiti"]};
            return rezultatFunkcije;
        }

        var konacnaPromjena =  (padaju1ne2 + padaju2)/(padaju1ne2 + brojTestova2)*100; 
    
        var tekstoviGresaka1 = [];
        greske1.forEach((element) => {
          tekstoviGresaka1.push(element.fullTitle);
        });
        tekstoviGresaka1.sort();
       //tekstoviGresaka2 - greske koje se pojavljuju u rezultatu2
        var greske2 = rezultat2Json.failures;
        var tekstoviGresaka2 =  [];
        greske2.forEach((element) => {
          tekstoviGresaka2.push(element.fullTitle);
        });
        tekstoviGresaka2.sort();

        rezultatFunkcije = {"promjena": konacnaPromjena.toString()+"%", "greske": tekstoviGresaka1.concat(tekstoviGresaka2)};
        return rezultatFunkcije;
      }catch(err){
          rezultatFunkcije =  { "promjena": "0%", "greske": ["Testovi se ne mogu izvršiti"] };
          return rezultatFunkcije;
      }
    }

  return {
    dajTacnost: dajTacnost,
    porediRezultate: porediRezultate
  };
})();
