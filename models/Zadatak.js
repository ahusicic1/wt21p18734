const Sequelize = require("sequelize");

module.exports = function(sequelize,DataTypes){
    const Zadatak = sequelize.define("Zadatak",{
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
          }
    ,
    tekst: Sequelize.STRING
})
    return Zadatak;
};